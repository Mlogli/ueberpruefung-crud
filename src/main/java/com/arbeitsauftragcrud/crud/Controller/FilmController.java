package com.arbeitsauftragcrud.crud.Controller;

import com.arbeitsauftragcrud.crud.Exceptions.RessourceNotFoundException;
import com.arbeitsauftragcrud.crud.Model.Film;
import com.arbeitsauftragcrud.crud.Model.Kinokarte;
import com.arbeitsauftragcrud.crud.Service.FilmService;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class FilmController {
    private FilmService filmService;

    @Autowired
    public FilmController(FilmService filmService){
        this.filmService = filmService;
    }

    @PostMapping
    public Film addFilm(@RequestBody @Valid @NotNull Film film){
        return filmService.addFilm(film);
    }
    @PostMapping("/addKinokarte")
    public Kinokarte addKinokarte(@RequestBody @Valid @NotNull Kinokarte kinokarte){
        return filmService.addKinokarte(kinokarte);
    }

    @PostMapping("{id}/addKinokarteToFilm")
    public ResponseEntity<Kinokarte> addKinokarteToFilm(@PathVariable Long id, @RequestBody @Valid Kinokarte kinokarte) throws RessourceNotFoundException {
        return ResponseEntity.ok(filmService.addKinoKarteToFilm(id, kinokarte));
    }

    @GetMapping("{id}/kinokarten")
    public ResponseEntity<List<Kinokarte>> getKinokartenByFilmId(@PathVariable Long id){
        return ResponseEntity.ok(filmService.getKinoKartenByFilmId(id));
    }

    @GetMapping("{running}/kinokartenRunning")
    public ResponseEntity<List<Kinokarte>> getKinokartenByFilmRunning(@PathVariable Boolean running){
        return ResponseEntity.ok(filmService.getKinoKartenByFilmRunning(running));
    }


}
