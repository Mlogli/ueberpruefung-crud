package com.arbeitsauftragcrud.crud.Repository;

import com.arbeitsauftragcrud.crud.Model.Kinokarte;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface KinokarteRepo extends JpaRepository<Kinokarte, Long> {


    List<Kinokarte> findAllByFilmId(Long id);
    List<Kinokarte> findAllByFilmRunning(Boolean running);
}
