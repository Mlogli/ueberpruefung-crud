package com.arbeitsauftragcrud.crud.Exceptions;

public class RessourceNotFoundException extends Exception {
    public RessourceNotFoundException(String message){
        super(message);
    }
}
