package com.arbeitsauftragcrud.crud.Model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Kinokarte {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "film_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Film film;

    @Id
    @GeneratedValue
    public Long id;

    @NotNull
    @Size(min=5, max=50)
    public String filmName;
    public boolean paid;
}
