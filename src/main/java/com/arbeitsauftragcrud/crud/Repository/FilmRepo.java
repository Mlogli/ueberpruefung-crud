package com.arbeitsauftragcrud.crud.Repository;

import com.arbeitsauftragcrud.crud.Model.Film;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FilmRepo extends JpaRepository<Film, Long> {

}
