package com.arbeitsauftragcrud.crud.Model;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Film {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Size(min=5, max=50)
    private String filmName;

    private float laufzeit;
    private boolean running;
}
