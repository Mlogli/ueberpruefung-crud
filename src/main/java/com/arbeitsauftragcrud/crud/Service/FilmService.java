package com.arbeitsauftragcrud.crud.Service;

import com.arbeitsauftragcrud.crud.Exceptions.RessourceNotFoundException;
import com.arbeitsauftragcrud.crud.Model.Film;
import com.arbeitsauftragcrud.crud.Model.Kinokarte;
import com.arbeitsauftragcrud.crud.Repository.FilmRepo;
import com.arbeitsauftragcrud.crud.Repository.KinokarteRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FilmService {
    private FilmRepo filmRepo;
    private KinokarteRepo kinokarteRepo;

    @Autowired
    public FilmService(FilmRepo filmRepo, KinokarteRepo kinokarteRepo){
        this.filmRepo = filmRepo;
        this.kinokarteRepo = kinokarteRepo;
    }
    public Film addFilm(Film film){
        return filmRepo.save(film);
    }
    public Kinokarte addKinokarte(Kinokarte kinokarte){ return kinokarteRepo.save(kinokarte);}
    public List<Film> saveFilms(List<Film> films){
        return filmRepo.saveAll(films);
    }

    public Kinokarte addKinoKarteToFilm(Long film_id, Kinokarte kinokarte) throws RessourceNotFoundException {
        Optional<Film> optFilm = filmRepo.findById(film_id);

        if(optFilm.isPresent()){
            Film film = optFilm.get();
            kinokarte.setFilm(film);
            kinokarteRepo.save(kinokarte);
            return kinokarte;
        }
        else{
            throw new RessourceNotFoundException("Nichts gefunden für "+ kinokarte.getId());
        }
    }

    public List<Kinokarte> getKinoKartenByFilmId(Long film_id){
        return kinokarteRepo.findAllByFilmId(film_id);
    }

    public List<Kinokarte> getKinoKartenByFilmRunning(Boolean running){


        return kinokarteRepo.findAllByFilmRunning(running);
    }


}
